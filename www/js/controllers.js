angular.module('FireWeight.controllers', ['LocalStorageModule', 'gettext'])

.run([
    '$rootScope', 'gettextCatalog', 'localStorageService', 'compute',
    function($rootScope, gettextCatalog, localStorageService, compute) {
        gettextCatalog.currentLanguage = navigator.language;

        //localStorageService.clearAll();
        $rootScope.dateFormat = moment.langData(moment.lang(navigator.language))._longDateFormat.L;
        $rootScope.weights = localStorageService.get('weights') || [];
        $rootScope.settings = localStorageService.get('settings') || {unity: 'metric'};
        $rootScope.unities = {
            imperial: {
                height: {name: 'inches', symbol: 'in', factor: 2.54, precision: 2},
                weight: {name: 'pounds', symbol: 'lb', factor: 0.45359237, precision: 2}
            },
            metric: {
                height: {name: 'cm', symbol: 'cm', factor: 1, precision: 1},
                weight: {name: 'kg', symbol: 'kg', factor: 1, precision: 1}
            }
        };

        $rootScope.isRightSideMenuEnabled = true;

        compute.renderData();
}])

.controller('AppCtrl', function($rootScope, $scope, $filter, $ionicListDelegate, $ionicNavBarDelegate,
                                $ionicTabsDelegate, $ionicSideMenuDelegate,
                                gettextCatalog, localStorageService, compute, conv) {
    $ionicNavBarDelegate.setTitle(gettextCatalog.getString('Tracking'));
    $scope.ctx = true

    $scope.add = function($event) {
        if (!$scope.weight.value || !$scope.weight.date) {
            return;
        }
        var weight = {};
        // use ISO format to store date
        weight.date = moment($scope.weight.date, $rootScope.dateFormat).format('YYYY-MM-DD');
        // use metric sytem to store value
        weight.value = conv.weightTo($scope.weight.value, $rootScope.settings.unity, 'metric');
        $rootScope.weights.push(weight);
        localStorageService.add('weights', $rootScope.weights);
        compute.renderData();
        $ionicListDelegate.closeOptionButtons();
        $ionicSideMenuDelegate.toggleRight();
    };

    $scope.delete_ = function() {
        $rootScope.weights.splice($scope.editIndex, 1);
        compute.renderData();
        localStorageService.add('weights', $rootScope.weights);
        $ionicListDelegate.closeOptionButtons();
        $ionicSideMenuDelegate.toggleRight();
    };

    $scope.edit = function($index) {
        $scope.ctx = false;
        $scope.editIndex = $rootScope.weights.length - $index - 1;
        $scope.weight = angular.copy($rootScope.weights[$scope.editIndex]);
        $scope.weight.date = moment($scope.weight.date, 'YYYY-MM-DD').format($rootScope.dateFormat);
        $scope.weight.value = conv.weight($scope.weight.value);
        $ionicListDelegate.closeOptionButtons();
        //$ionicSideMenuDelegate.canDragContent(true);
        $ionicSideMenuDelegate.toggleRight();
    };

    $scope.openDatePicker = function($event) {
        if (!window.plugins || !window.plugins.datePicker) {
            return;
        }
        var options = {
            mode: 'date',
            date: new Date(moment($($event.target).val(), $rootScope.dateFormat).format('YYYY-MM-DD'))
        };

        window.plugins.datePicker.show(options, function(date) {
            var dateFormatted;
            if (isNaN(Date.parse(date)) === false) {
                dateFormatted = moment(date.getTime() / 1000, 'X').format($rootScope.dateFormat);
                $scope.weight.date = dateFormatted;
                $($event.target).val(dateFormatted);
            }
        });
    };

    $scope.new_ = function() {
        $scope.ctx = true;
        $scope.weight = {
            value: $rootScope.weights.length ? conv.weight($rootScope.weights[$rootScope.weights.length-1].value) : '',
            date: moment().format($rootScope.dateFormat)
        };
        $ionicListDelegate.closeOptionButtons();
        $ionicSideMenuDelegate.toggleRight();
    };

    $scope.selectTab = function(index) {
        if (index === $ionicTabsDelegate.selectedIndex()) {
            return;
        }
        if (index === 0) {
            $ionicNavBarDelegate.changeTitle(gettextCatalog.getString('Tracking'), 'back');
        }
        else {
            $ionicNavBarDelegate.changeTitle(gettextCatalog.getString('Dashboard'), 'forward');
        }
        $ionicTabsDelegate.select(index);
    };

    $scope.update = function($event) {
        if (!$scope.weight.value || !$scope.weight.date) {
            return;
        }
        var weight = {};
        // use ISO format to store date
        weight.date = moment($scope.weight.date, $rootScope.dateFormat).format('YYYY-MM-DD');
        // use metric sytem to store value
        weight.value = conv.weightTo($scope.weight.value, $rootScope.settings.unity, 'metric');
        $rootScope.weights[$scope.editIndex] = weight;
        localStorageService.add('weights', $rootScope.weights);
        compute.renderData();
        $ionicListDelegate.closeOptionButtons();
        $ionicSideMenuDelegate.toggleRight();
    };

    $scope.updateValue = function(step) {
        $scope.weight.value = parseFloat(($scope.weight.value + step).toFixed(1));
    };
})

.controller('TrackingCtrl', function ($rootScope, $scope, conv) {
    $rootScope.isRightSideMenuEnabled = true;
    $scope.data = {
        progress: null,
        lostWeight: null,
        dailyAverage: null
    };

    if ($rootScope.weights.length) {
        $scope.$watch('weights', function(v) {
            var nb = v.length;
            if ($rootScope.settings.goalWeight) {
                $scope.data.lostWeight = Math.max(v[0].value - v[nb - 1].value, 0);
                $scope.data.progress = ($scope.data.lostWeight / (v[0].value - $rootScope.settings.goalWeight)) * 100;
            }
            $scope.data.dailyAverage = conv.weight((v[0].value - v[nb - 1].value) / nb);
            $scope.data.lostWeight = conv.weight($scope.data.lostWeight);
        });
    }
})

.controller('WeightCtrl', function($rootScope, $scope, $stateParams, conv) {
    $rootScope.isRightSideMenuEnabled = false;
    $scope.weight = $rootScope.weightsTracking[$stateParams.id];
    $scope.timeAgo = moment($scope.weight.date, 'YYYY-MM-DD').fromNow();
    $scope.lostWeight = conv.weight($rootScope.weights[0].value - $scope.weight.value);
    $scope.startWeight = conv.weight($rootScope.weights[0].value);
    if ($rootScope.settings.goalWeight) {
        $scope.toGo = conv.weight(
            conv.weightTo($scope.weight.value, $rootScope.settings.unity, 'metric') - $rootScope.settings.goalWeight
        );
        $scope.goalWeight = conv.weight($rootScope.settings.goalWeight);
        $scope.goalProgress = (Math.max($scope.lostWeight, 0) / ($scope.startWeight - $scope.goalWeight)) * 100;
    }
    else {
        $scope.toGo = '-';
        $scope.goalWeight = '';
    }
})

.controller('CalculatorCtrl', function($rootScope, $scope, $ionicModal, $ionicNavBarDelegate, $ionicTabsDelegate,
                                       gettextCatalog, compute, conv) {
    $rootScope.isRightSideMenuEnabled = false;
    $scope.bmi = {};
    $scope.bfp = {};
    $ionicNavBarDelegate.setTitle(gettextCatalog.getString('BMI Calculator'));

    $ionicModal.fromTemplateUrl(
        'templates/information.html',
        {
            scope: $scope,
            animation: 'slide-in-up'
        }
    ).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.estimateBFP = function($event) {
        if ($scope.bfp.sex && $scope.bfp.age && $scope.bfp.weight && $scope.bfp.height) {
            $scope.bfp.value = compute.BFP(
                compute.BMI(
                    conv.heightTo($scope.bfp.height, $rootScope.settings.unity, 'metric'),
                    conv.weightTo($scope.bfp.weight, $rootScope.settings.unity, 'metric')
                ),
                $scope.bfp.age,
                $scope.bfp.sex
            );
        }
        else {
            $scope.bfp.value = null;
        }
    };

    $scope.estimateBMI = function($event) {
        if ($scope.bmi.weight && $scope.bmi.height) {
            $scope.bmi.value = compute.BMI(
                conv.heightTo($scope.bmi.height, $rootScope.settings.unity, 'metric'),
                conv.weightTo($scope.bmi.weight, $rootScope.settings.unity, 'metric')
            );
        }
        else {
            $scope.bmi.value = null;
        }
    };

    $scope.selectTab = function(index) {
        if (index === $ionicTabsDelegate.selectedIndex()) {
            return;
        }
        if (index === 0) {
            $ionicNavBarDelegate.changeTitle(gettextCatalog.getString('BMI Calculator'), 'back');
        }
        else {
            $ionicNavBarDelegate.changeTitle(gettextCatalog.getString('BFP Calculator'), 'forward');
        }
        $ionicTabsDelegate.select(index);
    };
})

.controller('SettingsCtrl', function ($rootScope, $scope, $ionicModal, localStorageService, compute, conv) {
    $rootScope.isRightSideMenuEnabled = false;
    $scope.data = {
        sex: $rootScope.settings.sex,
        birthDate: $rootScope.settings.birthDate ? moment($rootScope.settings.birthDate, 'YYYY-MM-DD').format($rootScope.dateFormat) : '',
        unity: $rootScope.settings.unity,
        height: $rootScope.settings.height ? conv.height($rootScope.settings.height) : '',
        goalWeight: $rootScope.settings.goalWeight ? conv.weight($rootScope.settings.goalWeight) : '',
        goalBMI: $rootScope.settings.goalBMI || ''
    };

    $ionicModal.fromTemplateUrl(
        'templates/information.html',
        {
            scope: $scope,
            animation: 'slide-in-up'
        }
    ).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.onGoalBMIChange = function() {
        if ($scope.data.height && $scope.data.goalBMI) {
            $scope.data.goalWeight = conv.weight(
                compute.weightFromBMI(
                    conv.heightTo($scope.data.height, $scope.data.unity, 'metric'),
                    $scope.data.goalBMI
                )
            );
        }
        $scope.save();
    };

    $scope.onGoalWeightOrHeightChange = function() {
        if ($scope.data.height && $scope.data.goalWeight) {
            $scope.data.goalBMI = compute.BMI(
                conv.heightTo($scope.data.height, $scope.data.unity, 'metric'),
                conv.weightTo($scope.data.goalWeight, $scope.data.unity, 'metric')
            );
        }
        $scope.save();
    };

    $scope.onUnityChange = function() {
        $rootScope.settings.unity = $scope.data.unity;
        localStorageService.add('settings', $rootScope.settings);

        $scope.data.height = conv.height($rootScope.settings.height);
        $scope.data.goalWeight = conv.weight($rootScope.settings.goalWeight);

        compute.renderData();
    };

    $scope.openDatePicker = function($event) {
        if (!window.plugins || !window.plugins.datePicker) {
            return;
        }
        var options = {
            mode: 'date'
        };
        if ($($event.target).val()) {
            options.date = new Date(moment($($event.target).val(), $rootScope.dateFormat).format('YYYY-MM-DD'));
        }
        else {
            options.date = new Date();
        }
        window.plugins.datePicker.show(options, function(date) {
            var dateFormatted;
            if (isNaN(Date.parse(date)) === false) {
                dateFormatted = moment(date.getTime() / 1000, 'X').format($rootScope.dateFormat);
                $scope.data.birthDate = dateFormatted;
                $($event.target).val(dateFormatted);
                $scope.save();
            }
        });
    };

    $scope.save = function() {
        $rootScope.settings = {
            birthDate: moment($scope.data.birthDate, $rootScope.dateFormat).format('YYYY-MM-DD'),
            sex: $scope.data.sex,
            unity: $scope.data.unity,
            height: conv.heightTo($scope.data.height, $scope.data.unity, 'metric'),
            goalWeight: conv.weightTo($scope.data.goalWeight, $scope.data.unity, 'metric'),
            goalBMI: $scope.data.goalBMI
        };
        localStorageService.add('settings', $rootScope.settings);
        compute.renderData();
    };
});
