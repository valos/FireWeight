angular.module('FireWeight.services', [])

.service('conv', [
    '$rootScope',
    function($rootScope) {
        var self = this;

        // convert an height in metric system (cm) into specified unity system
        // if unity is not provided, use unity defined in settings
        this.height = function (value, to) {
            if (!to) {
                to = $rootScope.settings.unity;
            }
            unity = $rootScope.unities[to].height;
            return self.toFloat(new Big(value).div(unity.factor).toFixed(20), unity.precision);
        };

        // convert an height: metric <=> imperial
        this.heightTo = function(value, from, to) {
            var unity = $rootScope.unities.imperial.height;
            if (from === to) {
                return value;
            }
            if (to === 'metric') {
                return new Big(value).times(unity.factor).toFixed(20);
            }
            else {
                return new Big(value).div(unity.factor).toFixed(20);
            }
        };

        // convert a weight in metric system (kg) into specified unity system
        // if unity is not provided, use unity defined in settings
        this.weight = function (value, to) {
            if (!to) {
                to = $rootScope.settings.unity;
            }
            unity = $rootScope.unities[to].weight;
            return self.toFloat(new Big(value).div(unity.factor).toFixed(20), unity.precision);
        };

        // convert a weight: metric <=> imperial
        this.weightTo = function(value, from, to) {
            var unity = $rootScope.unities.imperial.weight;
            if (from === to) {
                return value;
            }
            if (to === 'metric') {
                return new Big(value).times(unity.factor).toFixed(20);
            }
            else {
                return new Big(value).div(unity.factor).toFixed(20);
            }
        };

        this.toFloat = function(string, precision) {
            return parseFloat(parseFloat(string).toFixed(precision));
        };
    }
])

.service('compute', [
    '$rootScope',
    '$filter',
    'conv',
    function($rootScope, $filter, conv) {
        var self = this;

        this.BFP = function(bmi, age, sex) {
            var value;
            if (!age && !sex) {
                if (isNaN(Date.parse($rootScope.settings.birthDate)) === true || !$rootScope.settings.sex) {
                    return null;
                }
                age = moment().diff($rootScope.settings.birthDate, 'years');
                sex = $rootScope.settings.sex === 'male' ? 1 : 0;
            }
            else {
                sex = sex === 'male' ? 1 : 0;
            }
            if (age >= 15) {
                value = (1.20 * bmi) + (0.23 * age) - (10.8 * sex) - 5.4;
            }
            else {
                value = (1.51 * bmi) - (0.70 * age) - (3.6 * sex) + 1.4;
            }
            return parseFloat(value).toFixed(1);
        };

        this.BMI = function(height, weight) {
            return parseFloat((weight / (Math.pow(height / 100, 2))).toFixed(1));
        };

        this.weightFromBMI = function(height, bmi) {
            return parseFloat((bmi * (Math.pow(height / 100, 2))).toFixed(1));
        };

        this.renderData = function () {
            // sort weights by date, reverse order
            $rootScope.weights = $filter('orderBy')($rootScope.weights, 'date');
            $rootScope.weightsTracking = [];
            $rootScope.weightsDashboardWeight = [];
            $rootScope.weightsDashboardBMI = [];
            $rootScope.weightsDashboardBFP = [];

            var prevValue;
            $.each($rootScope.weights, function(i, weight) {
                var bfp, bmi, indicator, ts, variation;

                if ($rootScope.settings.height) {
                    bmi = self.BMI($rootScope.settings.height, weight.value);
                    bfp = self.BFP(bmi);
                    if (bmi >= 30) {
                        // obese (class I: moderate, class II: severe, class III: very severe)
                        indicator = 'assertive';
                    }
                    else if (bmi >= 25 && bmi < 30) {
                        // overweight
                        indicator = 'energized';
                    }
                    else if (bmi >= 18.5 && bmi < 25) {
                        // normal
                        indicator = 'balanced';
                    }
                    else if (bmi >= 16 && bmi < 18.5) {
                        // underweight (mild)
                        indicator = 'energized';
                    }
                    else if (bmi < 16) {
                        // underweight (moderate and severe)
                        indicator = 'assertive';
                    }
                }
                else {
                    bmi = null;
                    bfp = null;
                    indicator = 'none';
                }

                if (prevValue) {
                    if (prevValue < weight.value) {
                        variation = 'up';
                    }
                    else if (prevValue > weight.value) {
                        variation = 'down';
                    }
                    else {
                        variation = 'right';
                    }
                }
                else {
                    variation = null;
                }
                prevValue = weight.value;

                $rootScope.weightsTracking.push({
                    value: conv.weight(weight.value),
                    date: weight.date,
                    dateFormatted: moment(weight.date, 'YYYY-MM-DD').format($rootScope.dateFormat),
                    bfp: bfp,
                    bmi: bmi,
                    indicator: indicator,
                    variation: variation
                });

                ts = new Date(weight.date).getTime();
                $rootScope.weightsDashboardWeight.push([ts, conv.weight(weight.value)]);
                if (bmi) {
                    $rootScope.weightsDashboardBMI.push([ts, bmi]);
                }
                if (bfp) {
                    $rootScope.weightsDashboardBFP.push([ts, bfp]);
                }
            });

            $rootScope.weightsTracking = $filter('orderBy')($rootScope.weightsTracking, 'date', true);
        };
    }
]);
