// FireWeight App

angular.module(
    'FireWeight',
    ['ionic', 'FireWeight.controllers', 'FireWeight.directives', 'FireWeight.services']
)

.run(function($ionicPlatform, gettextCatalog) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/menu.html",
        controller: 'AppCtrl'
    })

    .state('app.tracking', {
        url: "/tracking",
        views: {
            'main' :{
                templateUrl: "templates/tracking.html",
                controller: 'TrackingCtrl'
            }
        }
    })

    .state('app.weight', {
        url: "/tracking/:id",
        views: {
            'main' :{
                templateUrl: "templates/weight.html",
                controller: 'WeightCtrl'
            }
        }
    })

    .state('app.calculator', {
        url: "/calculator",
        views: {
            'main' :{
                templateUrl: "templates/calculator.html",
                controller: 'CalculatorCtrl'
            }
        }
    })

    .state('app.settings', {
        url: "/settings",
        views: {
            'main' :{
                templateUrl: "templates/settings.html",
                controller: 'SettingsCtrl'
            }
        }
    })

    .state('app.about', {
        url: "/about",
        views: {
            'main' :{
                templateUrl: "templates/about.html"
            }
        }
    });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/tracking');
});
