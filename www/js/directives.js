var averageRangeColor = '#CEF7A5';
var goalColor = '#4C9ED9';
var valuesColor = '#CB4B4B';

angular.module('FireWeight.directives', [])

.directive('chartWeight', function($rootScope, $window, gettextCatalog, compute, conv) {
    return {
        restrict: 'E',
        link: function($scope, element, attrs) {
            var chart;
            var data;
            var options = {
                lines: {show: true},
                points: {show: true},
                xaxis: {
                    mode: 'time',
                    timeformat: '%m/%d',
                    tickLength: 0,
                    minTickSize: [1, "day"]
                },
                legend: {
                    backgroundOpacity: 0
                },
                grid: {
                    borderWidth: 0
                }
            };
            var v = $rootScope.weightsDashboardWeight;

            if (v.length === 0) {
                return;
            }
            data = [{
                data: v,
                label: gettextCatalog.getString('Weight'),
                color: valuesColor
            }];
            if ($rootScope.settings.goalWeight) {
                data.push({
                    data: [
                        [v[0][0], conv.weight($rootScope.settings.goalWeight)],
                        [v[v.length - 1][0], conv.weight($rootScope.settings.goalWeight)]
                    ],
                    label: gettextCatalog.getString('Goal'),
                    points: {show: false},
                    color: goalColor
                });
            }
            if ($rootScope.settings.height) {
                data.push({
                    data: [],
                    label: gettextCatalog.getString('BMI') + ' 18.5-25',
                    points: {show: false},
                    lines: {show: false},
                    color: averageRangeColor
                });
                // used markings to display a rectangular area representing BMI average range (18.5-25)
                options.grid.markings = [{
                    color: averageRangeColor,
                    yaxis: {
                        from: conv.weight(compute.weightFromBMI($rootScope.settings.height, 18.5)),
                        to: conv.weight(compute.weightFromBMI($rootScope.settings.height, 25))
                    }
                }];
            }
            chart = $.plot(element, data, options);
        }
    };
})

.directive('chartBmi', function($rootScope, $window, gettextCatalog, conv) {
    return {
        restrict: 'E',
        link: function($scope, element, attrs) {
            var chart = null;
            var data;
            var options = {
                lines: {show: true},
                points: {show: true},
                xaxis: {
                    mode: 'time',
                    timeformat: '%m/%d',
                    tickLength: 0,
                    minTickSize: [1, "day"]
                },
                legend: {
                    backgroundOpacity: 0
                },
                grid: {
                    borderWidth: 0,
                    markings: [{color: averageRangeColor, yaxis: {from: 18.5, to: 25}}]
                }
            };
            var v = $rootScope.weightsDashboardBMI;

            if (v.length === 0) {
                return;
            }
            data = [
                {
                    data: [],
                    label: gettextCatalog.getString('BMI') + ' 18.5-25',
                    points: {show: false},
                    lines: {show: false},
                    color: averageRangeColor
                },
                {
                    data: v,
                    label: gettextCatalog.getString('BMI'),
                    color: valuesColor
                }
            ];
            if ($rootScope.settings.goalBMI) {
                data.push({
                    data: [
                        [v[0][0], $rootScope.settings.goalBMI],
                        [v[v.length - 1][0], $rootScope.settings.goalBMI]
                    ],
                    label: gettextCatalog.getString('Goal'),
                    points: {show: false},
                    color: goalColor
                });
            }
            chart = $.plot(element, data, options);
        }
    };
})

.directive('chartBfp', function($rootScope, $window, gettextCatalog, compute, conv) {
    return {
        restrict: 'E',
        link: function($scope, element, attrs) {
            var chart = null;
            var data;
            var averageRange = {
                'female': {from: 25, to: 31},
                'male': {from: 18, to: 24}
            }[$rootScope.settings.sex];
            var options = {
                lines: {show: true},
                points: {show: true},
                xaxis: {
                    mode: 'time',
                    timeformat: '%m/%d',
                    tickLength: 0,
                    minTickSize: [1, "day"]
                },
                legend: {
                    backgroundOpacity: 0
                },
                grid: {
                    borderWidth: 0,
                    markings: [{color: averageRangeColor, yaxis: averageRange}]
                }
            };
            var v = $rootScope.weightsDashboardBFP;

            if (v.length === 0) {
                return;
            }
            data = [
                {
                    data: [],
                    label: gettextCatalog.getString('BFP') + ' ' + averageRange.from + '-' + averageRange.to,
                    points: {show: false},
                    lines: {show: false},
                    color: averageRangeColor
                },
                {
                    data: v,
                    label: gettextCatalog.getString('BFP'),
                    color: valuesColor
                }
            ];
            if ($rootScope.settings.goalBMI) {
                data.push({
                    data: [
                        [v[0][0], compute.BFP($rootScope.settings.goalBMI)],
                        [v[v.length - 1][0], compute.BFP($rootScope.settings.goalBMI)]
                    ],
                    label: gettextCatalog.getString('Goal'),
                    points: {show: false},
                    color: goalColor
                });
            }
            chart = $.plot(element, data, options);
        }
    };
})

.directive('circle', function($rootScope, conv) {
    return {
        restrict: 'E',
        link: function($scope, element, attrs) {
            Circles.create({
                id: attrs.id,
                radius: 40,
                value: attrs.value,
                maxValue: 100,
                width: 10,
                text: function(value) {
                    return value + '%';
                },
                colors: ['#BCE3F8', '#3EADED'],
                duration: 200
            });
        }
    };
});
