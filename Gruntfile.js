module.exports = function (grunt) {

    grunt.initConfig({
        nggettext_extract: {
            pot: {
                options: {
                    extensions: {
                        html: 'html',
                        js: 'js'
                    }
                },
                files: {
                    'po/template.pot': [
                        'www/templates/*.html',
                        'www/js/*.js'
                    ]
                }
            }
        },
        nggettext_compile: {
            all: {
                files: {
                    'www/js/translations.js': ['po/*.po']
                }
            }
        }
    });

    //grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-angular-gettext');

    grunt.registerTask('default', ['nggettext_extract', 'nggettext_compile']);
};
