FireWeight
=====================

A ionic-based HTML5 app to track your weight.

## Installation

```bash
$ sudo npm install -g ionic cordova

$ cordova plugin add https://github.com/driftyco/ionic-plugins-keyboard.git
$ cordova plugin add org.apache.cordova.console
$ cordova plugin add org.apache.cordova.device
$ cordova plugin add https://github.com/VitaliiBlagodir/cordova-plugin-datepicker

$ cordova platform up android
```

## Build package

```bash
$ ionic build android
```

## Contribute by adding or improving translations

```bash
$ npm install grunt-angular-gettext --save-dev
```

Next, follow the [angular-gettext translation guide](http://angular-gettext.rocketeer.be/dev-guide/translate/).

## Used frameworks and libraries

[Ionic Framework](http://ionicframework.com/) The beautiful, open source front-end framework for developing hybrid mobile apps with HTML5

[AngularJs](https://angularjs.org) Superheroic JavaScript MVW Framework

[angular-gettext](http://angular-gettext.rocketeer.be/) Super-simple translation support for AngularJS

[DatePicker Plugin](https://github.com/VitaliiBlagodir/cordova-plugin-datepicker/) for Cordova/PhoneGap 3.0 (iOS and Android)

[Flot](http://www.flotcharts.org/) Attractive JavaScript plotting for jQuery

[Moment.js](http://momentjs.com/) Javascript date library for parsing, validating, manipulating, and formatting dates

[big.js](https://github.com/MikeMcl/big.js) Small, fast JavaScript library for arbitrary-precision decimal arithmetic

[Circles](https://github.com/lugolabs/circles) Lightwheight JavaScript library that generates circular graphs in SVG

## Make a Donation

[![Flattr](http://api.flattr.com/button/flattr-badge-large.png)](https://flattr.com/submit/auto?user_id=valos&url=https://github.com/valos/FireWeight) 

[![BitCoin](https://raw.github.com/valos/FireWeight/master/www/img/bitcoin.png)](bitcoin:15QKS2r4bwpoRUjbSyjgQxVtTAiUYLueSz?label=FireWeight&message=Donation%20to%20FireWeight "Donate using BitCoin") 15QKS2r4bwpoRUjbSyjgQxVtTAiUYLueSz
